package br.com.allin.ecommercefake;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.allin.ecommercefake.mvp.contract.LoginContract;
import br.com.allin.ecommercefake.mvp.presenter.LoginPresenter;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by lucasrodrigues on 06/03/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class LoginTest {
    @Mock LoginContract.View view;

    LoginContract.Presenter presenter;

    @Before
    public void bind() {
        presenter = spy(new LoginPresenter(view));
    }

    @Test
    public void loginEmpty() {
        String login = "";
        String password = "1234";

        presenter.login(login, password);

        verify(view).errorLogin(R.string.txt_error_login_empty);
    }

    @Test
    public void loginInvalid() {
        String login = "teste@teste";
        String password = "1234";

        presenter.login(login, password);

        verify(view).errorLogin(R.string.txt_error_invalid_login);
    }

    @Test
    public void passwordEmpty() {
        String login = "teste@teste.com";
        String password = "";

        presenter.login(login, password);

        verify(view).errorPassword(R.string.txt_error_password_empty);
    }

    @Test
    public void passwordInvalid() {
        String login = "teste@teste.com";
        String password = "12";

        presenter.login(login, password);

        verify(view).errorPassword(R.string.txt_error_invalid_password);
    }

    @Test
    public void invalidLogin() {
        String login = "teste@teste.com";
        String password = "1234";

        presenter.login(login, password);

        verify(view).invalidLogin(R.string.txt_attention, R.string.txt_error_login);
    }

    @Test
    public void loginValid() {
        String login = "lucasbrsilva@gmail.com";
        String password = "12345";

        presenter.login(login, password);

        verify(view).openOrder();
    }
}
