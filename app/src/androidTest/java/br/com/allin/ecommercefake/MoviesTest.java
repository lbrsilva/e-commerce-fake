package br.com.allin.ecommercefake;

import androidx.room.Room;
import android.content.Context;
import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.GenreListEntity;
import br.com.allin.ecommercefake.entities.MovieEntity;
import br.com.allin.ecommercefake.entities.MovieListEntity;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract;
import br.com.allin.ecommercefake.mvp.module.ServiceModule;
import br.com.allin.ecommercefake.mvp.presenter.MoviesPresenter;
import br.com.allin.ecommercefake.services.MoviesService;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by lucasrodrigues on 20/02/18.
 */

/**
    Android Room - Is initialized with "inMemoryDatabaseBuilder" because the database is simulated
    Mock.spy - Is used to create a object in Mock type
    RxAndroid/Java - Immediate is deprecated, but was recreated by me
 */
@RunWith(MockitoJUnitRunner.class)
public class MoviesTest {
    // View is the class used in MVP (Model-View-Presenter) that represent the interaction of the user interface
    @Mock MoviesContract.View view;

    // Service (Retrofit) and Database (Android ROM)
    AppDatabase appDatabase;
    MoviesService moviesService;

    // Presenter is the class used in MVP (Model-View-Presenter) that has the contract between Model and View
    MoviesPresenter moviesPresenter;

    @Before
    public void bind() {
        // Initialize Mockito ======================================================================
        MockitoAnnotations.initMocks(this);

        // Initializing Room DB ====================================================================
        appDatabase = Room.inMemoryDatabaseBuilder(mock(Context.class), AppDatabase.class).build();

        // Initializing Retrofit ===================================================================
        moviesService = spy(new ServiceModule()).configureRetrofit().create(MoviesService.class);

        // Initialize the Presenter ================================================================
        moviesPresenter = spy(new MoviesPresenter(view));
        moviesPresenter.setAppDatabase(appDatabase);
        moviesPresenter.setMoviesService(moviesService);

        bindScheduler();
    }

    private void bindScheduler() {
        // Creating my custom immediate ============================================================
        Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(Runnable run, long delay, TimeUnit unit) {
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        // Subscribe the values used in RXJava and in RXAndroid to my Custom Immediate =============
        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitComputationSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }

    // Testing - Load of Genres with Retrofit ======================================================
    @Test
    public void loadFlowGenres() {
        moviesPresenter.loadGenres();

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view).toggleLoad(View.VISIBLE);
        inOrder.verify(view).toggleLoad(View.GONE);
        inOrder.verify(view).showGenres(any(GenreListEntity.class));
    }

    @Test
    public void loadGenres() {
        TestObserver<GenreListEntity> testObserver = moviesService.genres().test();
        testObserver.awaitTerminalEvent();

        testObserver.assertValue(genreList ->
            genreList != null && genreList.getGenres() != null && genreList.getGenres().size() > 0);
    }

    // Testing - Load of Movies with Retrofit ======================================================
    @Test
    public void loadFlowMovies() {
        moviesPresenter.loadMovies(0, 28);

        verify(view).showMovies(anyInt(), any(MovieListEntity.class));
    }

    @Test
    public void loadMovies() {
        TestObserver<MovieListEntity> testObserver = moviesService.movies(28).test();
        testObserver.awaitTerminalEvent();

        testObserver.assertValue(movieList ->
            movieList.getResults() != null && movieList.getResults().size() > 0);
    }

    @Test
    public void loadFlowMoviesError() {
        moviesPresenter.loadMovies(0, -100);

        verify(view).showAlert(R.string.txt_attention, R.string.txt_error);
    }

    @Test
    public void loadMoviesError() {
        TestObserver<MovieListEntity> testObserver = moviesService.movies(-100).test();
        testObserver.awaitTerminalEvent();

        testObserver.assertError(Exception.class);
    }

    // Testing - Search Movies =====================================================================
    @Test
    public void insertMovies() {
        List<MovieEntity> list = new ArrayList<>();

        list.add(new MovieEntity(1, 3.5f, "Pantera", "TESTE"));
        list.add(new MovieEntity(2, 4.5f, "Brazil", "TESTE"));

        appDatabase.getMoviesDatabase().insert(list);
    }

    @Test
    public void searchMovieDatabase() {
        insertMovies();

        appDatabase.getMoviesDatabase().findByTitle("Pantera")
                .test()
                .assertNoErrors()
                .assertValue(movies -> movies != null && movies.size() > 0);
    }

    @Test
    public void searchMovieEmptyDatabase() {
        insertMovies();

        appDatabase.getMoviesDatabase().findByTitle("Blablabla")
                .test()
                .assertValue(movies -> movies == null || movies.size() == 0);
    }

    @Test
    public void searchMovie() {
        insertMovies();

        moviesPresenter.loadMoviesByQuery("Pantera");

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view).toggleMoviesEmpty(View.GONE);
        inOrder.verify(view).showMoviesByQuery(anyList());
    }

    @Test
    public void searchMovieEmpty() {
        insertMovies();

        moviesPresenter.loadMoviesByQuery("BLABLABLA");

        InOrder inOrder = Mockito.inOrder(view);
        inOrder.verify(view).toggleMoviesEmpty(View.VISIBLE);
        inOrder.verify(view).showMoviesByQuery(anyList());
    }
}