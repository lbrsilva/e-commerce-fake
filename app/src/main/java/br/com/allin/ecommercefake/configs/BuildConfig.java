package br.com.allin.ecommercefake.configs;

/**
 * Created by lucasrodrigues on 20/02/18.
 */

public interface BuildConfig {
    String DB_NAME = "ecommerce-fake";

    String BASE_URL = "https://api.themoviedb.org/3/";
    String API_KEY = "0d0ed06f1335ea981bf483412b8a9cc5";

    long TIME_OUT = 2;

    boolean DEBUG = br.com.allin.ecommercefake.BuildConfig.DEBUG;
}
