package br.com.allin.ecommercefake.mvp.presenter;

import java.util.List;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.GenreListEntity;
import br.com.allin.ecommercefake.entities.MovieEntity;
import br.com.allin.ecommercefake.entities.MovieListEntity;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract.Model;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract.Presenter;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract.View;
import br.com.allin.ecommercefake.mvp.model.MoviesModel;
import br.com.allin.ecommercefake.services.MoviesService;
import br.com.allin.ecommercefake.views.base.BasePresenter;

/**
 * Created by lbrsilva on 16/02/2018.
 */

public class MoviesPresenter extends BasePresenter implements Presenter {
    private View view;
    private Model model;

    public MoviesPresenter(View view) {
        this.view = view;
        this.model = new MoviesModel(this);
    }

    // INJECT SERVICE AND DATABASE =================================================================

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        model.setAppDatabase(appDatabase);
    }

    @Override
    public void setMoviesService(MoviesService moviesService) {
        model.setMoviesService(moviesService);
    }

    // VIEW CONTRACTS ==============================================================================

    @Override
    public void toggleLoad(int visibility) {
        view.toggleLoad(visibility);
    }

    @Override
    public void showMoviesByQuery(List<MovieEntity> movies) {
        view.showMoviesByQuery(movies);
    }

    @Override
    public void toggleMoviesEmpty(int visibility) {
        view.toggleMoviesEmpty(visibility);
    }

    @Override
    public void showGenres(GenreListEntity genreList) {
        view.showGenres(genreList);
    }

    @Override
    public void showMovies(int position, MovieListEntity movieList) {
        view.showMovies(position, movieList);
    }

    @Override
    public void showAlert(int title, int message) {
        view.showAlert(title, message);
    }

    @Override
    public void loadGenres() {
        mDisposable.add(model.loadGenres());
    }

    // MODEL CONTRACTS =============================================================================

    @Override
    public void loadMovies(int position, int genre) {
        mDisposable.add(model.loadMovies(position, genre));
    }

    @Override
    public void loadMoviesByQuery(String value) {
        mDisposable.add(model.loadMoviesByQuery(value));
    }

    @Override
    public void saveMovies(List<MovieEntity> movies) {
        model.saveMovies(movies);
    }
}
