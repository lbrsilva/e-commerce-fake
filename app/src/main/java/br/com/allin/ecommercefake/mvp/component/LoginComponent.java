package br.com.allin.ecommercefake.mvp.component;

import javax.inject.Singleton;

import br.com.allin.ecommercefake.mvp.module.LoginModule;
import br.com.allin.ecommercefake.views.activities.LoginActivity;
import dagger.Component;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

@Singleton
@Component(modules = { LoginModule.class })
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}