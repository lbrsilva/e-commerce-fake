package br.com.allin.ecommercefake.views.activities;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import javax.inject.Inject;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.mvp.component.DaggerOrderComponent;
import br.com.allin.ecommercefake.mvp.contract.OrderContract.View;
import br.com.allin.ecommercefake.mvp.module.OrderModule;
import br.com.allin.ecommercefake.mvp.presenter.OrderPresenter;
import br.com.allin.ecommercefake.views.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasrodrigues on 05/02/18.
 */

public class OrderActivity extends BaseActivity implements View {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tvPurchaseNumber) TextView tvPurchaseNumber;

    @Inject OrderPresenter orderPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_order);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            toolbar.setNavigationOnClickListener(view -> onBackPressed());
            toolbar.setTitle(R.string.txt_go_to_cart);
        }

        orderPresenter.setAppDatabase(getApp().getAppDatabase());
        orderPresenter.finalizeOrder();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        orderPresenter.dispose();
    }

    @Override
    public void showOrder(String value) {
        tvPurchaseNumber.setText(value);
    }

    @Override
    public void bind() {
        ButterKnife.bind(this);

        DaggerOrderComponent.Builder builder = DaggerOrderComponent.builder();
        builder.orderModule(new OrderModule(this));
        builder.build().inject(this);
    }
}
