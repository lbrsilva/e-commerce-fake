package br.com.allin.ecommercefake.mvp.model;

import br.com.allin.ecommercefake.ECommerceFakeApplication;
import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.entities.FavoriteEntity;
import br.com.allin.ecommercefake.entities.WarnEntity;
import br.com.allin.ecommercefake.helpers.RxUtils;
import br.com.allin.ecommercefake.mvp.contract.DetailsContract.Model;
import br.com.allin.ecommercefake.mvp.contract.DetailsContract.Presenter;
import br.com.allin.ecommercefake.services.MoviesService;
import br.com.allin.mobile.pushnotification.BTG360;
import br.com.allin.mobile.pushnotification.entity.btg.AIProduct;
import br.com.allin.mobile.pushnotification.entity.btg.AIWarn;
import br.com.allin.mobile.pushnotification.entity.btg.AIWish;
import io.reactivex.Completable;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public class DetailsModel implements Model {
    private Presenter presenter;
    private MoviesService moviesService;
    private AppDatabase appDatabase;

    public DetailsModel(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setMoviesService(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public Disposable load(int id) {
        BTG360.addProduct(ECommerceFakeApplication.BTG_CODE, new AIProduct(String.valueOf(id)));

        return moviesService.movieDetail(id)
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(movieDetail -> {
                    presenter.showMovieDetails(movieDetail);
                }, throwable -> {
                    presenter.showAlert(R.string.txt_attention, R.string.txt_error);
                });
    }

    @Override
    public Disposable addCart(CartEntity cart) {
        return Completable.fromAction(() -> appDatabase.getCartDatabase().insert(cart))
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(() -> presenter.itemAddedToCart());
    }

    @Override
    public void existInWarns(int id) {
        appDatabase.getWarnMeDatabase().isWarn(id)
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(new SingleObserver<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(Integer integer) {
                        presenter.showWarnButton(integer > 0);
                    }

                    @Override
                    public void onError(Throwable e) {
                        presenter.showWarnButton(false);
                    }
                });
    }

    @Override
    public Disposable addWarn(WarnEntity warn) {
        return Completable.fromAction(() -> appDatabase.getWarnMeDatabase().insert(warn))
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(() -> {
                    AIWarn warnMe = new AIWarn(String.valueOf(warn.getId()), true);
                    BTG360.addWarnMe(ECommerceFakeApplication.BTG_CODE, warnMe);

                    presenter.showWarnButton(true);
                    presenter.showAlert(R.string.txt_attention, R.string.txt_alert_warn);
                });
    }

    @Override
    public Disposable removeWarn(WarnEntity warn) {
        return Completable.fromAction(() -> appDatabase.getWarnMeDatabase().delete(warn))
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(() -> {
                    AIWarn warnMe = new AIWarn(String.valueOf(warn.getId()), false);
                    BTG360.addWarnMe(ECommerceFakeApplication.BTG_CODE, warnMe);

                    presenter.showWarnButton(false);
                });
    }

    @Override
    public void existInFavorites(int id) {
        appDatabase.getFavoriteDatabase().isFavorite(id)
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(new SingleObserver<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(Integer integer) {
                        presenter.showFavoriteButton(integer > 0);
                    }

                    @Override
                    public void onError(Throwable e) {
                        presenter.showFavoriteButton(false);
                    }
                });
    }

    @Override
    public Disposable addFavorite(FavoriteEntity favorite) {
        return Completable.fromAction(() -> appDatabase.getFavoriteDatabase().insert(favorite))
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(() -> {
                    AIWish wish = new AIWish(String.valueOf(favorite.getId()), true);
                    BTG360.addWishList(ECommerceFakeApplication.BTG_CODE, wish);

                    presenter.showFavoriteButton(true);
                });
    }

    @Override
    public Disposable removeFavorite(FavoriteEntity favorite) {
        return Completable.fromAction(() -> appDatabase.getFavoriteDatabase().delete(favorite))
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(() -> {
                    AIWish wish = new AIWish(String.valueOf(favorite.getId()), false);
                    BTG360.addWishList(ECommerceFakeApplication.BTG_CODE, wish);

                    presenter.showFavoriteButton(false);
                });
    }
}
