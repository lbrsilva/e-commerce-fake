package br.com.allin.ecommercefake.mvp.presenter;

import br.com.allin.ecommercefake.ECommerceFakeApplication;
import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.mvp.contract.OrderContract.Model;
import br.com.allin.ecommercefake.mvp.contract.OrderContract.Presenter;
import br.com.allin.ecommercefake.mvp.contract.OrderContract.View;
import br.com.allin.ecommercefake.mvp.model.OrderModel;
import br.com.allin.ecommercefake.views.base.BasePresenter;
import br.com.allin.mobile.pushnotification.BTG360;
import br.com.allin.mobile.pushnotification.entity.btg.AITransaction;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public class OrderPresenter extends BasePresenter implements Presenter {
    private View view;
    private Model model;

    public OrderPresenter(View view) {
        this.view = view;
        this.model = new OrderModel(this);
    }

    // INJECT DATABASE =============================================================================

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        model.setAppDatabase(appDatabase);
    }

    // VIEW CONTRACTS ==============================================================================

    @Override
    public void showOrder() {
        int min = 100000;
        int max = 999999;
        int number = min + (int)( Math.random() * ((max - min) + 1));

        String id = String.valueOf(number);
        String order = "PIUI-" + String.valueOf(number);

        BTG360.addTransaction(ECommerceFakeApplication.BTG_CODE, new AITransaction(id, order));

        view.showOrder(order);
    }

    // MODEL CONTRACTS =============================================================================

    @Override
    public void finalizeOrder() {
        mDisposable.add(model.truncate());
    }

}
