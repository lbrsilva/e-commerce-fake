package br.com.allin.ecommercefake.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.helpers.ImageHelper;
import br.com.allin.ecommercefake.mvp.presenter.CartPresenter;

/**
 * Created by lucasrodrigues on 02/02/18.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {
    private CartPresenter cartPresenter;
    private List<CartEntity> cartList;

    public void setPresenter(CartPresenter cartPresenter) {
        this.cartPresenter = cartPresenter;
    }

    public void setCartList(List<CartEntity> cartList) {
        this.cartList = cartList;

        notifyDataSetChanged();
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.adapter_cart, parent, false);

        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CartViewHolder holder, int position) {
        CartEntity cart = cartList.get(position);

        Glide.with(holder.getContext()).load(ImageHelper.getURL(cart.getImage())).into(holder.ivCover);

        holder.tvTitle.setText(cart.getTitle());
        holder.itemView.setOnLongClickListener(view -> {
            cartPresenter.alertConfirmDelete(position);

            return false;
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    class CartViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivCover;
        private TextView tvTitle;

        CartViewHolder(View itemView) {
            super(itemView);

            ivCover = itemView.findViewById(R.id.ivCover);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }

        Context getContext() {
            return itemView.getContext();
        }
    }
}
