package br.com.allin.ecommercefake.mvp.module;

import br.com.allin.ecommercefake.mvp.contract.OrderContract.View;
import br.com.allin.ecommercefake.mvp.presenter.OrderPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

@Module
public class OrderModule {
    private View view;

    public OrderModule(View view) {
        this.view = view;
    }

    @Provides
    OrderPresenter getOrderPresenter() {
        return new OrderPresenter(view);
    }
}
