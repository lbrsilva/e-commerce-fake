package br.com.allin.ecommercefake.mvp.component;

import javax.inject.Singleton;

import br.com.allin.ecommercefake.mvp.module.MoviesModule;
import br.com.allin.ecommercefake.mvp.module.ServiceModule;
import br.com.allin.ecommercefake.views.activities.MainActivity;
import dagger.Component;

/**
 * Created by lucasrodrigues on 20/02/18.
 */

@Singleton
@Component(modules = { ServiceModule.class, MoviesModule.class })
public interface MoviesComponent {
    void inject(MainActivity activity);
}
