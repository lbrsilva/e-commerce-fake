package br.com.allin.ecommercefake.mvp.contract;

import androidx.annotation.StringRes;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public interface LoginContract {
    interface View {
        void openOrder();

        void errorLogin(@StringRes int message);

        void errorPassword(@StringRes int message);

        void invalidLogin(@StringRes int title, @StringRes int message);
    }

    interface Presenter extends Model, View {
    }

    interface Model {
        void login(String user, String password);
    }
}
