package br.com.allin.ecommercefake.mvp.model;

import android.text.TextUtils;
import android.util.Patterns;

import br.com.allin.ecommercefake.ECommerceFakeApplication;
import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.mvp.contract.LoginContract.Model;
import br.com.allin.ecommercefake.mvp.presenter.LoginPresenter;
import br.com.allin.mobile.pushnotification.BTG360;
import br.com.allin.mobile.pushnotification.entity.btg.AIClient;

/**
 * Created by lucasrodrigues on 06/03/18.
 */

public class LoginModel implements Model {
    private LoginPresenter loginPresenter;

    public LoginModel(LoginPresenter loginPresenter) {
        this.loginPresenter = loginPresenter;
    }

    @Override
    public void login(String user, String password) {
        if (TextUtils.isEmpty(user)) {
            loginPresenter.errorLogin(R.string.txt_error_login_empty);
        } else if (!Patterns.EMAIL_ADDRESS.matcher(user).matches()) {
            loginPresenter.errorLogin(R.string.txt_error_invalid_login);
        } else if (TextUtils.isEmpty(password)) {
            loginPresenter.errorPassword(R.string.txt_error_password_empty);
        } else if (password.length() <= 3) {
            loginPresenter.errorPassword(R.string.txt_error_invalid_password);
        } else if (!user.equalsIgnoreCase("lucasbrsilva@gmail.com") && !password.equals("12345")) {
            loginPresenter.invalidLogin(R.string.txt_attention, R.string.txt_error_login);
        } else {
            BTG360.addClient(ECommerceFakeApplication.BTG_CODE, new AIClient(user));

            loginPresenter.openOrder();
        }
    }
}
