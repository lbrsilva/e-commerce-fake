package br.com.allin.ecommercefake.helpers;

import android.app.AlertDialog;
import android.content.Context;

import br.com.allin.ecommercefake.R;


/**
 * Created by lucasrodrigues on 31/01/18.
 */

public class Alert {
    public static void error(Context context, int title, int message) {
        Alert.error(context, context.getString(title), context.getString(message));
    }

    public static void error(Context context, String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(R.string.txt_ok, (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.create().show();
    }
}
