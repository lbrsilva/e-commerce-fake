package br.com.allin.ecommercefake.mvp.model;

import java.util.List;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.helpers.RxUtils;
import br.com.allin.ecommercefake.mvp.contract.CartContract.Model;
import br.com.allin.ecommercefake.mvp.contract.CartContract.Presenter;
import io.reactivex.Completable;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public class CartModel implements Model {
    private Presenter presenter;
    private AppDatabase appDatabase;

    public CartModel(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public void loadCartItems() {
        appDatabase.getCartDatabase().getAll()
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(new SingleObserver<List<CartEntity>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(List<CartEntity> cartEntities) {
                        presenter.showCartItems(cartEntities);
                    }

                    @Override
                    public void onError(Throwable e) {
                        presenter.showCartEmpty();
                    }
                });
    }

    @Override
    public Disposable deleteCartItem(int position, CartEntity cart) {
        return Completable.fromAction(() -> appDatabase.getCartDatabase().delete(cart))
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(() -> presenter.updateItemRemoved(position));
    }
}
