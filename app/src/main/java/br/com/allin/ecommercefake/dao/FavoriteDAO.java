package br.com.allin.ecommercefake.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import br.com.allin.ecommercefake.entities.FavoriteEntity;
import io.reactivex.Single;

/**
 * Created by lucasrodrigues on 05/02/18.
 */

@Dao
public interface FavoriteDAO {
    @Query("SELECT COUNT(*) FROM favorites WHERE ID = :id")
    Single<Integer> isFavorite(int id);

    @Insert
    void insert(FavoriteEntity favorite);

    @Delete
    void delete(FavoriteEntity favorite);
}
