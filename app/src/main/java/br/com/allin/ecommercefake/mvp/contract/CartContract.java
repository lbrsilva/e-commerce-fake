package br.com.allin.ecommercefake.mvp.contract;

import java.util.List;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.CartEntity;
import io.reactivex.disposables.Disposable;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public interface CartContract {
    interface View {
        void showCartItems(List<CartEntity> cart);

        void showCartEmpty();

        void updateItemRemoved(int position);

        void alertConfirmDelete(int position);

        void alertPurchaseCompleted();

        void errorPurchase();

        void startLogin();
    }

    interface Presenter extends View {
        void loadCartItems();

        void finalizePurchase();

        void deleteCartItem(int position);

        void setAppDatabase(AppDatabase appDatabase);
    }

    interface Model {
        void loadCartItems();

        Disposable deleteCartItem(int position, CartEntity cart);

        void setAppDatabase(AppDatabase appDatabase);
    }
}
