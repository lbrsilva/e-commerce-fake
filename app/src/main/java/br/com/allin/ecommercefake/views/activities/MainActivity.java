package br.com.allin.ecommercefake.views.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.entities.GenreListEntity;
import br.com.allin.ecommercefake.entities.MovieEntity;
import br.com.allin.ecommercefake.entities.MovieListEntity;
import br.com.allin.ecommercefake.helpers.Alert;
import br.com.allin.ecommercefake.helpers.Divider;
import br.com.allin.ecommercefake.mvp.component.DaggerMoviesComponent;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract;
import br.com.allin.ecommercefake.mvp.module.MoviesModule;
import br.com.allin.ecommercefake.mvp.module.ServiceModule;
import br.com.allin.ecommercefake.mvp.presenter.MoviesPresenter;
import br.com.allin.ecommercefake.services.MoviesService;
import br.com.allin.ecommercefake.views.adapters.GenresAdapter;
import br.com.allin.ecommercefake.views.adapters.MoviesAdapter;
import br.com.allin.ecommercefake.views.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lbrsilva on 16/02/2018.
 */
public class MainActivity extends BaseActivity implements MoviesContract.View,
        OnNavigationItemSelectedListener, OnQueryTextListener, OnActionExpandListener {
    // It' not possible to inject because it's in the internal menu
    private SearchView searchView;

    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.rvMovies) RecyclerView rvMovies;
    @BindView(R.id.pbMovies) ProgressBar pbMovies;
    @BindView(R.id.tvMoviesEmpty) TextView tvMoviesEmpty;

    @Inject MoviesService moviesService;
    @Inject GenresAdapter genresAdapter;
    @Inject MoviesAdapter moviesAdapter;
    @Inject MoviesPresenter moviesPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        setup();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        moviesPresenter.dispose();
    }

    private void setup() {
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        drawer.addDrawerListener(toggle);
        navigationView.setNavigationItemSelectedListener(this);

        rvMovies.setItemViewCacheSize(20);
        rvMovies.setDrawingCacheEnabled(true);
        rvMovies.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvMovies.addItemDecoration(new Divider((int) getResources().getDimension(R.dimen.divider)));

        genresAdapter.setPresenter(moviesPresenter);
        moviesPresenter.setAppDatabase(getApp().getAppDatabase());
        moviesPresenter.setMoviesService(moviesService);

        rvMovies.setAdapter(genresAdapter);
        moviesPresenter.loadGenres();
    }

    @Override
    public void showGenres(GenreListEntity genres) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        rvMovies.setLayoutManager(layoutManager);
        genresAdapter.setGenreList(genres);
    }

    @Override
    public void showMovies(int position, MovieListEntity movieList) {
        genresAdapter.notifyItemChanged(position, movieList);
    }

    @Override
    public void showMoviesByQuery(List<MovieEntity> movies) {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);

        rvMovies.setLayoutManager(layoutManager);
        rvMovies.setAdapter(moviesAdapter);

        moviesAdapter.setMovieList(movies);
    }

    @Override
    public void toggleMoviesEmpty(int visibility) {
        tvMoviesEmpty.setVisibility(visibility);
    }

    @Override
    public void showAlert(int title, int message) {
        Alert.error(this, title, message);
    }

    @Override
    public void toggleLoad(int visibility) {
        pbMovies.setVisibility(visibility);
    }

    // BACK BUTTON CLICK ===========================================================================

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    // NAVIGATION AND CREATION MENU ================================================================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem menuItem = menu.findItem(R.id.search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) menuItem.getActionView();
        searchView.onActionViewExpanded();
        searchView.setQueryRefinementEnabled(true);
        searchView.setOnQueryTextListener(this);

        if (searchManager != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        menuItem.setOnActionExpandListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.cart) {
            startActivity(new Intent(this, CartActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    // QUERY IN SEARCH VIEW ========================================================================

    @Override
    public boolean onQueryTextSubmit(String value) {
        moviesPresenter.loadMoviesByQuery(value);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        searchView.setQuery(null, false);
        searchView.setIconified(false);
        searchView.requestFocus();

        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        searchView.setIconified(true);
        searchView.clearFocus();

        rvMovies.setLayoutManager(new LinearLayoutManager(this));
        rvMovies.setAdapter(genresAdapter);

        return true;
    }

    // DEPENDENCY INJECTION ========================================================================

    @Override
    public void bind() {
        ButterKnife.bind(this);

        DaggerMoviesComponent.Builder builder = DaggerMoviesComponent.builder();
        builder.serviceModule(new ServiceModule(new File(getCacheDir(), "responses")));
        builder.moviesModule(new MoviesModule(this));
        builder.build().inject(this);
    }
}
