package br.com.allin.ecommercefake.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;

import javax.inject.Inject;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.helpers.Alert;
import br.com.allin.ecommercefake.mvp.component.DaggerLoginComponent;
import br.com.allin.ecommercefake.mvp.contract.LoginContract;
import br.com.allin.ecommercefake.mvp.module.LoginModule;
import br.com.allin.ecommercefake.mvp.presenter.LoginPresenter;
import br.com.allin.ecommercefake.views.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasrodrigues on 05/02/18.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View {
    @Inject LoginPresenter loginPresenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.etLogin) TextInputEditText etLogin;
    @BindView(R.id.etPassword) TextInputEditText etPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            toolbar.setNavigationOnClickListener(view -> onBackPressed());
            toolbar.setTitle(R.string.txt_login);
        }
    }

    public void clickBack(View view) {
        finish();
    }

    public void clickGo(View view) {
        loginPresenter.login(etLogin.getText().toString(), etPassword.getText().toString());
    }

    @Override
    public void errorLogin(int message) {
        etLogin.setError(getString(message));
    }

    @Override
    public void errorPassword(int message) {
        etPassword.setError(getString(message));
    }

    @Override
    public void invalidLogin(@StringRes int title, @StringRes int message) {
        Alert.error(this, title, message);
    }

    @Override
    public void openOrder() {
        startActivity(new Intent(LoginActivity.this, OrderActivity.class));
        finish();
    }

    @Override
    public void bind() {
        ButterKnife.bind(this);

        DaggerLoginComponent.Builder builder = DaggerLoginComponent.builder();
        builder.loginModule(new LoginModule(this));
        builder.build().inject(this);
    }
}
