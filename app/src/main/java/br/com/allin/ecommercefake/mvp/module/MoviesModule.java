package br.com.allin.ecommercefake.mvp.module;

import br.com.allin.ecommercefake.mvp.contract.MoviesContract.View;
import br.com.allin.ecommercefake.mvp.presenter.MoviesPresenter;
import br.com.allin.ecommercefake.views.adapters.GenresAdapter;
import br.com.allin.ecommercefake.views.adapters.MoviesAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasrodrigues on 21/02/18.
 */

@Module
public class MoviesModule {
    private View view;

    public MoviesModule(View view) {
        this.view = view;
    }

    @Provides
    GenresAdapter getGenresAdapter() {
        return new GenresAdapter();
    }

    @Provides
    MoviesAdapter getMoviesAdapter() {
        return new MoviesAdapter();
    }

    @Provides
    MoviesPresenter getMoviesPresenter() {
        return new MoviesPresenter(view);
    }
}
