package br.com.allin.ecommercefake.mvp.module;

import androidx.room.Room;

import br.com.allin.ecommercefake.ECommerceFakeApplication;
import br.com.allin.ecommercefake.configs.BuildConfig;
import br.com.allin.ecommercefake.dao.AppDatabase;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasrodrigues on 22/02/18.
 */

@Module
public class DatabaseModule {
    private ECommerceFakeApplication application;

    public DatabaseModule(ECommerceFakeApplication application) {
        this.application = application;
    }

    @Provides
    public AppDatabase getAppDatabase() {
        return Room.databaseBuilder(application, AppDatabase.class, BuildConfig.DB_NAME).build();
    }
}
