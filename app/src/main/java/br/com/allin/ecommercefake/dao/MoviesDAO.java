package br.com.allin.ecommercefake.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import br.com.allin.ecommercefake.entities.MovieEntity;
import io.reactivex.Single;

/**
 * Created by lbrsilva on 15/02/2018.
 */

@Dao
public interface MoviesDAO {
    @Query("SELECT * FROM MOVIES WHERE TITLE LIKE '%' || :value || '%'")
    Single<List<MovieEntity>> findByTitle(String value);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<MovieEntity> list);
}
