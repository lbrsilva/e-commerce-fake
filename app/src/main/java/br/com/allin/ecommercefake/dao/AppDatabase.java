package br.com.allin.ecommercefake.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.entities.FavoriteEntity;
import br.com.allin.ecommercefake.entities.MovieEntity;
import br.com.allin.ecommercefake.entities.WarnEntity;

/**
 * Created by lucasrodrigues on 22/02/18.
 */

@Database(
        version = 1,
        entities = {
                CartEntity.class,
                FavoriteEntity.class,
                MovieEntity.class,
                WarnEntity.class
        },
        exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CartDAO getCartDatabase();

    public abstract FavoriteDAO getFavoriteDatabase();

    public abstract MoviesDAO getMoviesDatabase();

    public abstract WarnMeDAO getWarnMeDatabase();
}
