package br.com.allin.ecommercefake.mvp.module;

import br.com.allin.ecommercefake.mvp.contract.LoginContract.View;
import br.com.allin.ecommercefake.mvp.presenter.LoginPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

@Module
public class LoginModule {
    private View view;

    public LoginModule(View view) {
        this.view = view;
    }

    @Provides
    LoginPresenter getLoginPresenter() {
        return new LoginPresenter(view);
    }
}
