package br.com.allin.ecommercefake.services;

import br.com.allin.ecommercefake.entities.GenreListEntity;
import br.com.allin.ecommercefake.entities.MovieDetailEntity;
import br.com.allin.ecommercefake.entities.MovieListEntity;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

public interface MoviesService {
    @GET("genre/movie/list")
    Single<GenreListEntity> genres();

    @GET("genre/{genre_id}/movies?include_adult=false&sort_by=created_at.asc")
    Single<MovieListEntity> movies(@Path("genre_id") int genreId);

    @GET("movie/{movie_id}")
    Single<MovieDetailEntity> movieDetail(@Path("movie_id") int movieId);
}