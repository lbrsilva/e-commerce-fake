package br.com.allin.ecommercefake.mvp.model;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.allin.ecommercefake.ECommerceFakeApplication;
import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.MovieEntity;
import br.com.allin.ecommercefake.helpers.RxUtils;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract.Model;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract.Presenter;
import br.com.allin.ecommercefake.services.MoviesService;
import br.com.allin.mobile.pushnotification.BTG360;
import br.com.allin.mobile.pushnotification.entity.btg.AISearch;
import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lbrsilva on 16/02/2018.
 */

public class MoviesModel implements Model {
    private Presenter presenter;
    private AppDatabase appDatabase;
    private MoviesService moviesService;

    public MoviesModel(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setMoviesService(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public Disposable loadGenres() {
        presenter.toggleLoad(View.VISIBLE);

        return moviesService.genres()
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(genreList -> {
                    presenter.toggleLoad(View.GONE);
                    presenter.showGenres(genreList);
                }, throwable -> {
                    presenter.toggleLoad(View.GONE);
                    presenter.showAlert(R.string.txt_attention, R.string.txt_error);
                });
    }

    @Override
    public Disposable loadMovies(int position, final int genre) {
        return moviesService.movies(genre)
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(moviesList -> {
                    presenter.saveMovies(moviesList.getResults());
                    presenter.showMovies(position, moviesList);
                }, throwable -> {
                    presenter.showAlert(R.string.txt_attention, R.string.txt_error);
                });
    }

    @Override
    public Disposable loadMoviesByQuery(String value) {
        BTG360.addSearch(ECommerceFakeApplication.BTG_CODE, new AISearch(value));

        return appDatabase.getMoviesDatabase().findByTitle(value)
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(movies -> {
                    if (movies != null && movies.size() > 0) {
                        presenter.toggleMoviesEmpty(View.GONE);
                    } else {
                        presenter.toggleMoviesEmpty(View.VISIBLE);
                    }

                    presenter.showMoviesByQuery(movies);
                }, throwable -> {
                    presenter.toggleMoviesEmpty(View.VISIBLE);
                    presenter.showMoviesByQuery(new ArrayList<>());
                });
    }

    @Override
    public void saveMovies(List<MovieEntity> movies) {
        Completable.fromAction(() -> appDatabase.getMoviesDatabase().insert(movies))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }
}
