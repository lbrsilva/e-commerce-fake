package br.com.allin.ecommercefake.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import br.com.allin.ecommercefake.entities.CartEntity;
import io.reactivex.Single;

/**
 * Created by lucasrodrigues on 02/02/18.
 */

@Dao
public interface CartDAO {
    @Query("SELECT * FROM CART")
    Single<List<CartEntity>> getAll();

    @Delete
    void delete(CartEntity cart);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CartEntity cart);

    @Query("DELETE FROM CART")
    void truncate();
}
