package br.com.allin.ecommercefake.mvp.component;

import javax.inject.Singleton;

import br.com.allin.ecommercefake.mvp.module.CartModule;
import br.com.allin.ecommercefake.views.activities.CartActivity;
import dagger.Component;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

@Singleton
@Component(modules = CartModule.class)
public interface CartComponent {
    void inject(CartActivity cartActivity);
}
