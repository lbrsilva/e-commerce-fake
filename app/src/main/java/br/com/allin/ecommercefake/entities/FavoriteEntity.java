package br.com.allin.ecommercefake.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by lucasrodrigues on 22/02/18.
 */

@Entity(tableName = "favorites")
public class FavoriteEntity {
    @PrimaryKey
    private int id;

    public FavoriteEntity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
