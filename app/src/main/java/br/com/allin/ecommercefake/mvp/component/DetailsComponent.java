package br.com.allin.ecommercefake.mvp.component;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

import javax.inject.Singleton;

import br.com.allin.ecommercefake.mvp.module.DetailsModule;
import br.com.allin.ecommercefake.mvp.module.ServiceModule;
import br.com.allin.ecommercefake.views.activities.DetailsActivity;
import dagger.Component;

@Singleton
@Component(modules = { ServiceModule.class, DetailsModule.class })
public interface DetailsComponent {
    void inject(DetailsActivity detailsActivity);
}
