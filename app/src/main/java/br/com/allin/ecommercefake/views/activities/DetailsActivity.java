package br.com.allin.ecommercefake.views.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.drawable.DrawableCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.io.File;

import javax.inject.Inject;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.entities.MovieDetailEntity;
import br.com.allin.ecommercefake.helpers.Alert;
import br.com.allin.ecommercefake.helpers.ImageHelper;
import br.com.allin.ecommercefake.mvp.component.DaggerDetailsComponent;
import br.com.allin.ecommercefake.mvp.contract.DetailsContract;
import br.com.allin.ecommercefake.mvp.module.DetailsModule;
import br.com.allin.ecommercefake.mvp.module.ServiceModule;
import br.com.allin.ecommercefake.mvp.presenter.DetailsPresenter;
import br.com.allin.ecommercefake.services.MoviesService;
import br.com.allin.ecommercefake.views.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasrodrigues on 01/02/18.
 */

public class DetailsActivity extends BaseActivity implements DetailsContract.View {
    @BindView(R.id.ctlDetails) CollapsingToolbarLayout ctlDetails;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.ivMovie) ImageView ivMovie;
    @BindView(R.id.ivFavorite) ImageView ivFavorite;
    @BindView(R.id.ivCover) ImageView ivCover;
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.tvInfos) TextView tvInfos;
    @BindView(R.id.tvRating) TextView tvRating;
    @BindView(R.id.tvResume) TextView tvResume;
    @BindView(R.id.tvLanguage) TextView tvLanguage;

    @Inject MoviesService moviesService;
    @Inject DetailsPresenter detailsPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_details);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            toolbar.setNavigationOnClickListener(view -> onBackPressed());
        }

        ctlDetails.setCollapsedTitleTextColor(Color.WHITE);
        ctlDetails.setExpandedTitleColor(Color.TRANSPARENT);
        ivFavorite.setOnClickListener(view -> detailsPresenter.addOrRemoveFavorite());

        detailsPresenter.setAppDatabase(getApp().getAppDatabase());
        detailsPresenter.setMoviesService(moviesService);
        detailsPresenter.load(getIntent().getIntExtra(DetailsActivity.class.toString(), 0));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        detailsPresenter.dispose();
    }

    @Override
    public void showMovieDetails(MovieDetailEntity movieDetail) {
        String originalImage = ImageHelper.getOriginalURL(movieDetail.getPosterPath());
        String posterImage = ImageHelper.getURL(movieDetail.getPosterPath());

        Glide.with(this).load(originalImage).into(ivMovie);
        Glide.with(this).load(posterImage).into(ivCover);

        toolbar.setTitle(movieDetail.getTitle());
        tvTitle.setText(movieDetail.getTitle());
        tvInfos.setText(movieDetail.getReleaseDate());
        tvRating.setText(String.valueOf(movieDetail.getVoteAverage()));
        tvResume.setText(movieDetail.getOverview());
        tvLanguage.setText(movieDetail.getOriginalLanguage());

        detailsPresenter.existInFavorites(movieDetail.getId());
        detailsPresenter.existInWarns(movieDetail.getId());
    }

    public void clickBuy(View view) {
        detailsPresenter.addToCart();
    }

    public void clickWarn(View view) {
        detailsPresenter.addOrRemoveWarn();
    }

    @Override
    public void itemAddedToCart() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.txt_attention);
        alertDialogBuilder.setMessage(R.string.txt_item_add);
        alertDialogBuilder.setNegativeButton(R.string.txt_go_to_cart, (dialogInterface, i) -> {
            dialogInterface.dismiss();

            detailsPresenter.startCart();
        });
        alertDialogBuilder.setPositiveButton(R.string.txt_keep_buying, (dialogInterface, i) -> {
            dialogInterface.dismiss();

            finish();
        });
        alertDialogBuilder.create().show();
    }

    @Override
    public void showWarnButton(boolean enable) {
    }

    @Override
    public void showFavoriteButton(boolean enable) {
        int color = enable ? Color.YELLOW : Color.parseColor("#CCCCCC");
        Drawable drawable = DrawableCompat.wrap(ivFavorite.getDrawable());
        DrawableCompat.setTintList(drawable, ColorStateList.valueOf(color));

        ivFavorite.setImageDrawable(drawable);
    }

    @Override
    public void startCart() {
        startActivity(new Intent(this, CartActivity.class));
        finish();
    }

    @Override
    public void showAlert(int title, int message) {
        Alert.error(this, title, message);
    }

    @Override
    public void showToast(int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void bind() {
        ButterKnife.bind(this);

        DaggerDetailsComponent.Builder builder = DaggerDetailsComponent.builder();
        builder.detailsModule(new DetailsModule(this));
        builder.serviceModule(new ServiceModule(new File(getCacheDir(), "responses")));
        builder.build().inject(this);
    }
}
