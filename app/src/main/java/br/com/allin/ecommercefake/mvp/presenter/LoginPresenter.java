package br.com.allin.ecommercefake.mvp.presenter;

import androidx.annotation.StringRes;

import br.com.allin.ecommercefake.mvp.contract.LoginContract.Model;
import br.com.allin.ecommercefake.mvp.contract.LoginContract.Presenter;
import br.com.allin.ecommercefake.mvp.contract.LoginContract.View;
import br.com.allin.ecommercefake.mvp.model.LoginModel;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public class LoginPresenter implements Presenter {
    private Model model;
    private View view;

    public LoginPresenter(View view) {
        this.model = new LoginModel(this);
        this.view = view;
    }

    // VIEW CONTRACTS ==============================================================================

    @Override
    public void login(String user, String password) {
        model.login(user, password);
    }

    @Override
    public void invalidLogin(@StringRes int title, @StringRes int message) {
        view.invalidLogin(title, message);
    }

    @Override
    public void openOrder() {
        view.openOrder();
    }

    @Override
    public void errorLogin(@StringRes int message) {
        view.errorLogin(message);
    }

    @Override
    public void errorPassword(@StringRes int message) {
        view.errorPassword(message);
    }
}
