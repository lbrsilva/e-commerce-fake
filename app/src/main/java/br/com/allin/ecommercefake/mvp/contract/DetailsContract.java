package br.com.allin.ecommercefake.mvp.contract;

import androidx.annotation.StringRes;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.entities.FavoriteEntity;
import br.com.allin.ecommercefake.entities.MovieDetailEntity;
import br.com.allin.ecommercefake.entities.WarnEntity;
import br.com.allin.ecommercefake.services.MoviesService;
import io.reactivex.disposables.Disposable;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public interface DetailsContract {
    interface View {
        void showMovieDetails(MovieDetailEntity movieDetail);

        void startCart();

        void itemAddedToCart();

        void showToast(@StringRes int message);

        void showAlert(@StringRes int title, @StringRes int message);

        void showFavoriteButton(boolean enable);

        void showWarnButton(boolean enable);
    }

    interface Presenter extends View {
        void load(int id);

        void addToCart();

        void existInWarns(int id);

        void addOrRemoveWarn();

        void existInFavorites(int id);

        void addOrRemoveFavorite();

        void setMoviesService(MoviesService moviesService);

        void setAppDatabase(AppDatabase appDatabase);
    }

    interface Model {
        Disposable load(int id);

        Disposable addCart(CartEntity cart);

        void existInWarns(int id);

        Disposable addWarn(WarnEntity warn);

        Disposable removeWarn(WarnEntity warn);

        void existInFavorites(int id);

        Disposable addFavorite(FavoriteEntity favorite);

        Disposable removeFavorite(FavoriteEntity favorite);

        void setMoviesService(MoviesService moviesService);

        void setAppDatabase(AppDatabase appDatabase);
    }
}
