package br.com.allin.ecommercefake.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import br.com.allin.ecommercefake.entities.WarnEntity;
import io.reactivex.Single;

/**
 * Created by lucasrodrigues on 05/02/18.
 */

@Dao
public interface WarnMeDAO {
    @Query("SELECT * FROM warns WHERE id = :id")
    Single<Integer> isWarn(int id);

    @Insert
    void insert(WarnEntity warnMe);

    @Delete
    void delete(WarnEntity warnMe);
}
