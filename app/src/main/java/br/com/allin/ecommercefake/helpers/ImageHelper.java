package br.com.allin.ecommercefake.helpers;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

public class ImageHelper {
    public static String getURL(String value) {
        return String.format("http://image.tmdb.org/t/p/w185%s", value);
    }

    public static String getOriginalURL(String value) {
        return String.format("http://image.tmdb.org/t/p/original%s", value);
    }
}