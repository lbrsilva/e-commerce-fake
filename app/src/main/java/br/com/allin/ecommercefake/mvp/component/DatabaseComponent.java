package br.com.allin.ecommercefake.mvp.component;

import android.app.Application;

import javax.inject.Singleton;

import br.com.allin.ecommercefake.mvp.module.DatabaseModule;
import dagger.Component;

/**
 * Created by lucasrodrigues on 22/02/18.
 */

@Singleton
@Component(modules = { DatabaseModule.class })
public interface DatabaseComponent {
    void inject(Application application);
}
