package br.com.allin.ecommercefake.views.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.helpers.Alert;
import br.com.allin.ecommercefake.mvp.component.DaggerCartComponent;
import br.com.allin.ecommercefake.mvp.contract.CartContract;
import br.com.allin.ecommercefake.mvp.module.CartModule;
import br.com.allin.ecommercefake.mvp.presenter.CartPresenter;
import br.com.allin.ecommercefake.views.adapters.CartAdapter;
import br.com.allin.ecommercefake.views.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasrodrigues on 02/02/18.
 */

public class CartActivity extends BaseActivity implements CartContract.View {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvCart)
    RecyclerView rvCart;
    @BindView(R.id.tvEmptyCart)
    TextView tvEmptyCart;

    @Inject
    CartPresenter cartPresenter;
    @Inject
    CartAdapter cartAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cart);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            toolbar.setNavigationOnClickListener(view -> onBackPressed());
            toolbar.setTitle(R.string.txt_go_to_cart);
        }

        cartPresenter.setAppDatabase(getApp().getAppDatabase());
        cartAdapter.setPresenter(cartPresenter);
        cartPresenter.loadCartItems();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        cartPresenter.dispose();
    }

    public void clickFinalize(View view) {
        cartPresenter.finalizePurchase();
    }

    @Override
    public void showCartItems(List<CartEntity> cart) {
        tvEmptyCart.setVisibility(View.GONE);
        rvCart.setVisibility(View.VISIBLE);

        cartAdapter.setCartList(cart);
        rvCart.setAdapter(cartAdapter);
    }

    @Override
    public void showCartEmpty() {
        tvEmptyCart.setVisibility(View.VISIBLE);
        rvCart.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updateItemRemoved(int position) {
        cartAdapter.notifyItemRemoved(position);
    }

    @Override
    public void alertConfirmDelete(int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.txt_attention);
        alertDialogBuilder.setMessage(R.string.txt_confirm_delete);
        alertDialogBuilder.setPositiveButton(R.string.txt_yes,
                (dialogInterface, i) -> cartPresenter.deleteCartItem(position));
        alertDialogBuilder.setNegativeButton(R.string.txt_no,
                (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.create().show();
    }

    @Override
    public void alertPurchaseCompleted() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.txt_attention);
        alertDialogBuilder.setMessage(R.string.txt_confirm_finalize);
        alertDialogBuilder.setPositiveButton(R.string.txt_yes, (dialogInterface, i) -> {
            dialogInterface.dismiss();

            cartPresenter.startLogin();
        });

        alertDialogBuilder.setNegativeButton(R.string.txt_no,
                (dialogInterface, i) -> dialogInterface.dismiss());
        alertDialogBuilder.create().show();
    }

    @Override
    public void errorPurchase() {
        Alert.error(this, R.string.txt_attention, R.string.txt_error_cart);
    }

    @Override
    public void startLogin() {
        startActivity(new Intent(CartActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void bind() {
        ButterKnife.bind(this);

        DaggerCartComponent.Builder builder = DaggerCartComponent.builder();
        builder.cartModule(new CartModule(this));
        builder.build().inject(this);
    }
}
