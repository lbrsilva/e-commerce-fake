package br.com.allin.ecommercefake.mvp.contract;

import androidx.annotation.StringRes;

import java.util.List;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.GenreListEntity;
import br.com.allin.ecommercefake.entities.MovieEntity;
import br.com.allin.ecommercefake.entities.MovieListEntity;
import br.com.allin.ecommercefake.services.MoviesService;
import io.reactivex.disposables.Disposable;

/**
 * Created by lbrsilva on 16/02/2018.
 */

public interface MoviesContract {
    interface View {
        void toggleLoad(int visibility);

        void showGenres(GenreListEntity genreList);

        void showMovies(int position, MovieListEntity movieList);

        void showMoviesByQuery(List<MovieEntity> movies);

        void toggleMoviesEmpty(int visibility);

        void showAlert(@StringRes int title, @StringRes int message);
    }

    interface Presenter extends View {
        void loadGenres();

        void loadMovies(int position, int genre);

        void loadMoviesByQuery(String value);

        void saveMovies(List<MovieEntity> movies);

        void setMoviesService(MoviesService moviesService);

        void setAppDatabase(AppDatabase appDatabase);
    }

    interface Model {
        Disposable loadGenres();

        Disposable loadMovies(int position, int genre);

        Disposable loadMoviesByQuery(String value);

        void saveMovies(List<MovieEntity> movies);

        void setMoviesService(MoviesService moviesService);

        void setAppDatabase(AppDatabase appDatabase);
    }
}
