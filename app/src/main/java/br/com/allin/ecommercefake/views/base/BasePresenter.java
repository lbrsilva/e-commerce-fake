package br.com.allin.ecommercefake.views.base;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter {
    protected CompositeDisposable mDisposable;

    protected BasePresenter() {
        this.mDisposable = new CompositeDisposable();
    }

    public void dispose() {
        mDisposable.dispose();
    }
}
