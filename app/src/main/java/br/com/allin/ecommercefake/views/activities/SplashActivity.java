package br.com.allin.ecommercefake.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import br.com.allin.ecommercefake.R;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

public class SplashActivity extends AppCompatActivity implements Runnable {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        new Thread(this).start();
    }

    @Override
    public void run() {
        SystemClock.sleep(1500);

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
