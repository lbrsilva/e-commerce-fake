package br.com.allin.ecommercefake.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by lucasrodrigues on 22/02/18.
 */

@Entity(tableName = "warns")
public class WarnEntity {
    @PrimaryKey
    private int id;

    public WarnEntity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}