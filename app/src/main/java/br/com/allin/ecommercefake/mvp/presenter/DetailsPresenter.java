package br.com.allin.ecommercefake.mvp.presenter;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.entities.FavoriteEntity;
import br.com.allin.ecommercefake.entities.MovieDetailEntity;
import br.com.allin.ecommercefake.entities.WarnEntity;
import br.com.allin.ecommercefake.mvp.contract.DetailsContract.Model;
import br.com.allin.ecommercefake.mvp.contract.DetailsContract.Presenter;
import br.com.allin.ecommercefake.mvp.contract.DetailsContract.View;
import br.com.allin.ecommercefake.mvp.model.DetailsModel;
import br.com.allin.ecommercefake.services.MoviesService;
import br.com.allin.ecommercefake.views.base.BasePresenter;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public class DetailsPresenter extends BasePresenter implements Presenter {
    private View view;
    private Model model;
    private MovieDetailEntity movieDetail;

    public DetailsPresenter(View view) {
        this.view = view;
        this.model = new DetailsModel(this);
    }

    // INJECT SERVICE AND DATABASE =================================================================

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        model.setAppDatabase(appDatabase);
    }

    @Override
    public void setMoviesService(MoviesService moviesService) {
        model.setMoviesService(moviesService);
    }

    // VIEW CONTRACTS ==============================================================================

    @Override
    public void showMovieDetails(MovieDetailEntity movieDetail) {
        this.movieDetail = movieDetail;

        view.showMovieDetails(movieDetail);
    }

    @Override
    public void itemAddedToCart() {
        view.itemAddedToCart();
    }

    @Override
    public void showToast(int message) {
        view.showToast(message);
    }

    @Override
    public void showAlert(int title, int message) {
        view.showAlert(title, message);
    }

    @Override
    public void showFavoriteButton(boolean enable) {
        movieDetail.setFavorite(enable);

        view.showFavoriteButton(enable);
    }

    @Override
    public void showWarnButton(boolean enable) {
        movieDetail.setWarn(enable);

        view.showWarnButton(enable);
    }

    @Override
    public void startCart() {
        view.startCart();
    }

    // MODEL CONTRACTS =============================================================================

    @Override
    public void load(int id) {
        mDisposable.add(model.load(id));
    }

    @Override
    public void addToCart() {
        mDisposable.add(model.addCart(new CartEntity(movieDetail.getId(),
                movieDetail.getTitle(), movieDetail.getPosterPath())));
    }


    @Override
    public void addOrRemoveFavorite() {
        if (movieDetail.isFavorite()) {
            mDisposable.add(model.removeFavorite(new FavoriteEntity(movieDetail.getId())));
        } else {
            mDisposable.add(model.addFavorite(new FavoriteEntity(movieDetail.getId())));
        }
    }

    @Override
    public void addOrRemoveWarn() {
        if (movieDetail.isWarn()) {
            mDisposable.add(model.removeWarn(new WarnEntity(movieDetail.getId())));
        } else {
            mDisposable.add(model.addWarn(new WarnEntity(movieDetail.getId())));
        }
    }

    @Override
    public void existInFavorites(int id) {
        model.existInFavorites(id);
    }

    @Override
    public void existInWarns(int id) {
        model.existInWarns(id);
    }
}
