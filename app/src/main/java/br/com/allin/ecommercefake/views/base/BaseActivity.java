package br.com.allin.ecommercefake.views.base;

import androidx.appcompat.app.AppCompatActivity;

import br.com.allin.ecommercefake.ECommerceFakeApplication;

/**
 * Created by lucasrodrigues on 20/02/18.
 */

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        bind();
    }

    protected abstract void bind();

    protected ECommerceFakeApplication getApp() {
        return (ECommerceFakeApplication) getApplication();
    }
}
