package br.com.allin.ecommercefake.mvp.module;

import br.com.allin.ecommercefake.mvp.contract.DetailsContract.View;
import br.com.allin.ecommercefake.mvp.presenter.DetailsPresenter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

@Module
public class DetailsModule {
    private View view;

    public DetailsModule(View view) {
        this.view = view;
    }

    @Provides
    DetailsPresenter getDetailsPresenter() {
        return new DetailsPresenter(view);
    }
}
