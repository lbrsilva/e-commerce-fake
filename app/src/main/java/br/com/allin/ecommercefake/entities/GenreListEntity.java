package br.com.allin.ecommercefake.entities;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenreListEntity {
    @SerializedName("genres")
    @Expose
    private List<GenreEntity> genres = null;

    public List<GenreEntity> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreEntity> genres) {
        this.genres = genres;
    }
}