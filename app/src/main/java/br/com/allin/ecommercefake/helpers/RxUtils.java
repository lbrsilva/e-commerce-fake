package br.com.allin.ecommercefake.helpers;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lucasrodrigues on 28/02/18.
 */

public class RxUtils {
    public static Scheduler getSubscriverOn() {
        return Schedulers.io();
    }

    public static Scheduler getObserveOn() {
        return AndroidSchedulers.mainThread();
    };
}
