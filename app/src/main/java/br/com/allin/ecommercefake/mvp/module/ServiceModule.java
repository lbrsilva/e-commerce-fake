package br.com.allin.ecommercefake.mvp.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.File;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import br.com.allin.ecommercefake.configs.BuildConfig;
import br.com.allin.ecommercefake.services.MoviesService;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

@Module
public class ServiceModule {
    private File cacheFile;

    public ServiceModule() {
    }

    public ServiceModule(File cacheFile) {
        this.cacheFile = cacheFile;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public MoviesService getService(Retrofit retrofit) {
        return retrofit.create(MoviesService.class);
    }

    @Provides
    @Singleton
    public Retrofit configureRetrofit() {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

        okHttpClientBuilder
                .connectTimeout(BuildConfig.TIME_OUT, TimeUnit.MINUTES)
                .writeTimeout(BuildConfig.TIME_OUT, TimeUnit.MINUTES)
                .readTimeout(BuildConfig.TIME_OUT, TimeUnit.MINUTES);

        okHttpClientBuilder.addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder();

            HttpUrl httpUrl = request.url().newBuilder()
                    .addQueryParameter("api_key", BuildConfig.API_KEY)
                    .addQueryParameter("language", Locale.getDefault().getLanguage())
                    .build();

            builder.url(httpUrl);

            Response response = chain.proceed(builder.build());
            response.cacheResponse();

            return response;
        });

        if (cacheFile != null) {
            okHttpClientBuilder.cache(new Cache(cacheFile, 10 * 1024 * 1024));
        }

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder.addInterceptor(logging);
        }

        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}