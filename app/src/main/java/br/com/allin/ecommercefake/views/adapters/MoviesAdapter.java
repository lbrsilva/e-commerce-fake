package br.com.allin.ecommercefake.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.entities.MovieEntity;
import br.com.allin.ecommercefake.helpers.ImageHelper;
import br.com.allin.ecommercefake.views.activities.DetailsActivity;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {
    private List<MovieEntity> movies;

    public void setMovieList(List<MovieEntity> movies) {
        this.movies = movies;

        notifyDataSetChanged();
    }

    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.adapter_movie, parent, false);

        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MoviesViewHolder holder, final int position) {
        final MovieEntity movie = movies.get(position);

        Glide.with(holder.getContext())
                .load(ImageHelper.getURL(movie.getPosterPath())).into(holder.ivMovie);

        holder.tvMovie.setText(movie.getTitle());
        holder.tvRating.setText(String.valueOf(movie.getVoteAverage()));
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(holder.getContext(), DetailsActivity.class);
            intent.putExtra(DetailsActivity.class.toString(), movie.getId());
            holder.getContext().startActivity(intent);
        });

        for (Drawable drawable : holder.tvRating.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(
                        Color.parseColor("#CCCCCC"), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class MoviesViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivMovie;
        private TextView tvMovie;
        private TextView tvRating;

        MoviesViewHolder(View itemView) {
            super(itemView);

            ivMovie = itemView.findViewById(R.id.ivMovie);
            tvMovie = itemView.findViewById(R.id.tvMovie);
            tvRating = itemView.findViewById(R.id.tvRating);
        }

        Context getContext() {
            return itemView.getContext();
        }
    }
}
