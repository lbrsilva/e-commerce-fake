package br.com.allin.ecommercefake.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lucasrodrigues on 31/01/18.
 */
@Entity(tableName = "movies")
public class MovieEntity implements Serializable {
    @Expose
    @SerializedName("id")
    @PrimaryKey
    private int id;
    @Expose
    @ColumnInfo(name = "vote_count")
    @SerializedName("vote_count")
    private int voteCount;
    @Expose
    @ColumnInfo(name = "video")
    @SerializedName("video")
    private boolean video;
    @Expose
    @ColumnInfo(name = "vote_average")
    @SerializedName("vote_average")
    private float voteAverage;
    @Expose
    @ColumnInfo(name = "title")
    @SerializedName("title")
    private String title;
    @Expose
    @ColumnInfo(name = "popularity")
    @SerializedName("popularity")
    private double popularity;
    @Expose
    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    private String posterPath;
    @Expose
    @ColumnInfo(name = "original_language")
    @SerializedName("original_language")
    private String originalLanguage;
    @Expose
    @ColumnInfo(name = "original_title")
    @SerializedName("original_title")
    private String originalTitle;
    @Expose
    @ColumnInfo(name = "backdrop_path")
    @SerializedName("backdrop_path")
    private String backdropPath;
    @Expose
    @ColumnInfo(name = "adult")
    @SerializedName("adult")
    private boolean adult;
    @Expose
    @ColumnInfo(name = "overview")
    @SerializedName("overview")
    private String overview;
    @Expose
    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    private String releaseDate;

    public MovieEntity() {
    }

    public MovieEntity(int id, float voteAverage, String title, String posterPath) {
        this.id = id;
        this.voteAverage = voteAverage;
        this.title = title;
        this.posterPath = posterPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}