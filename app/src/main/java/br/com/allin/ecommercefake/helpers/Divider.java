package br.com.allin.ecommercefake.helpers;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by lucasrodrigues on 01/02/18.
 */

public class Divider extends RecyclerView.ItemDecoration {

    private final int verticalSpaceHeight;

    public Divider(int verticalSpaceHeight) {
        this.verticalSpaceHeight = verticalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = verticalSpaceHeight;
    }
}