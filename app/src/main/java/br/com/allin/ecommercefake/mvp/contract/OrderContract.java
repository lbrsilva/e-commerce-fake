package br.com.allin.ecommercefake.mvp.contract;

import br.com.allin.ecommercefake.dao.AppDatabase;
import io.reactivex.disposables.Disposable;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public interface OrderContract {
    interface View {
        void showOrder(String value);
    }

    interface Presenter {
        void showOrder();

        void finalizeOrder();

        void setAppDatabase(AppDatabase appDatabase);
    }

    interface Model {
        Disposable truncate();

        void setAppDatabase(AppDatabase appDatabase);
    }
}
