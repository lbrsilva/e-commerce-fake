package br.com.allin.ecommercefake.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import java.util.List;

import br.com.allin.ecommercefake.R;
import br.com.allin.ecommercefake.entities.GenreEntity;
import br.com.allin.ecommercefake.entities.GenreListEntity;
import br.com.allin.ecommercefake.entities.MovieListEntity;
import br.com.allin.ecommercefake.mvp.contract.MoviesContract.Presenter;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

public class GenresAdapter extends RecyclerView.Adapter<GenresAdapter.GenresViewHolder> {
    private GenreListEntity genreList;
    private Presenter presenter;

    public void setGenreList(GenreListEntity genreList) {
        this.genreList = genreList;

        notifyDataSetChanged();
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public GenresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.adapter_genre, parent, false);

        return new GenresViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GenresViewHolder holder, final int position) {
        GenreEntity genre = genreList.getGenres().get(position);

        holder.tvGenre.setText(genre.getName());

        presenter.loadMovies(position, genre.getId());
    }

    @Override
    public void onBindViewHolder(final GenresViewHolder holder,
                                 final int position, final List<Object> movies) {
        if (movies == null || movies.size() == 0) {
            super.onBindViewHolder(holder, position, movies);
        } else {
            SnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(holder.rvMovies);
            LinearLayoutManager layoutManager = new LinearLayoutManager(holder.getContext());
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            MovieListEntity movieList = (MovieListEntity) movies.get(0);
            MoviesAdapter moviesAdapter = new MoviesAdapter();

            holder.rvMovies.setOnFlingListener(null);
            holder.rvMovies.setItemViewCacheSize(20);
            holder.rvMovies.setDrawingCacheEnabled(true);
            holder.rvMovies.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            holder.rvMovies.setAdapter(moviesAdapter);
            holder.rvMovies.setLayoutManager(layoutManager);

            moviesAdapter.setMovieList(movieList.getResults());
        }
    }

    @Override
    public int getItemCount() {
        return genreList.getGenres().size();
    }

    class GenresViewHolder extends RecyclerView.ViewHolder {
        private TextView tvGenre;
        private RecyclerView rvMovies;

        GenresViewHolder(View itemView) {
            super(itemView);

            tvGenre = itemView.findViewById(R.id.tvGenre);
            rvMovies = itemView.findViewById(R.id.rvMovies);
        }

        Context getContext() {
            return itemView.getContext();
        }
    }
}