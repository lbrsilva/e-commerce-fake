package br.com.allin.ecommercefake.mvp.component;

import javax.inject.Singleton;

import br.com.allin.ecommercefake.mvp.module.OrderModule;
import br.com.allin.ecommercefake.views.activities.OrderActivity;
import dagger.Component;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

@Singleton
@Component(modules = OrderModule.class)
public interface OrderComponent {
    void inject(OrderActivity orderActivity);
}
