package br.com.allin.ecommercefake.mvp.module;

import br.com.allin.ecommercefake.mvp.contract.CartContract.View;
import br.com.allin.ecommercefake.mvp.presenter.CartPresenter;
import br.com.allin.ecommercefake.views.adapters.CartAdapter;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

@Module
public class CartModule {
    private View view;

    public CartModule(View view) {
        this.view = view;
    }

    @Provides
    CartAdapter getCartAdapter() {
        return new CartAdapter();
    }

    @Provides
    CartPresenter getCartPresenter() {
        return new CartPresenter(view);
    }
}
