package br.com.allin.ecommercefake.mvp.presenter;

import java.util.ArrayList;
import java.util.List;

import br.com.allin.ecommercefake.ECommerceFakeApplication;
import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.entities.CartEntity;
import br.com.allin.ecommercefake.mvp.contract.CartContract.Model;
import br.com.allin.ecommercefake.mvp.contract.CartContract.Presenter;
import br.com.allin.ecommercefake.mvp.contract.CartContract.View;
import br.com.allin.ecommercefake.mvp.model.CartModel;
import br.com.allin.ecommercefake.views.base.BasePresenter;
import br.com.allin.mobile.pushnotification.BTG360;
import br.com.allin.mobile.pushnotification.entity.btg.AICart;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public class CartPresenter extends BasePresenter implements Presenter {
    private List<CartEntity> cartItems;
    private View view;
    private Model model;

    public CartPresenter(View view) {
        this.view = view;
        this.model = new CartModel(this);
    }

    // INJECT DATABASE =============================================================================

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        model.setAppDatabase(appDatabase);
    }

    // VIEW CONTRACTS ==============================================================================

    @Override
    public void showCartItems(List<CartEntity> cartItems) {
        this.cartItems = cartItems;

        List<AICart> carts = new ArrayList<>();

        for (CartEntity cart : cartItems) {
            carts.add(new AICart(String.valueOf(cart.getId())));
        }

        BTG360.addCarts(ECommerceFakeApplication.BTG_CODE, carts);

        view.showCartItems(cartItems);
    }

    @Override
    public void showCartEmpty() {
        view.showCartEmpty();
    }

    @Override
    public void updateItemRemoved(int position) {
        view.alertConfirmDelete(position);
    }

    @Override
    public void alertConfirmDelete(int position) {
        view.alertConfirmDelete(position);
    }

    @Override
    public void alertPurchaseCompleted() {
        view.alertPurchaseCompleted();
    }

    @Override
    public void errorPurchase() {
        view.errorPurchase();
    }

    @Override
    public void startLogin() {
        view.startLogin();
    }

    @Override
    public void finalizePurchase() {
        view.startLogin();
    }

    // MODEL CONTRACTS =============================================================================

    @Override
    public void loadCartItems() {
        model.loadCartItems();
    }

    @Override
    public void deleteCartItem(int position) {
        mDisposable.add(model.deleteCartItem(position, cartItems.get(position)));
    }
}
