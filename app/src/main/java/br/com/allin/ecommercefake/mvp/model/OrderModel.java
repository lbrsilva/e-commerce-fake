package br.com.allin.ecommercefake.mvp.model;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.helpers.RxUtils;
import br.com.allin.ecommercefake.mvp.contract.OrderContract.Model;
import br.com.allin.ecommercefake.mvp.contract.OrderContract.Presenter;
import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;

/**
 * Created by lucasrodrigues on 23/02/18.
 */

public class OrderModel implements Model {
    private Presenter presenter;
    private AppDatabase appDatabase;

    public OrderModel(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setAppDatabase(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public Disposable truncate() {
        return Completable.fromAction(() -> appDatabase.getCartDatabase().truncate())
                .subscribeOn(RxUtils.getSubscriverOn())
                .observeOn(RxUtils.getObserveOn())
                .subscribe(() -> presenter.showOrder());
    }
}
