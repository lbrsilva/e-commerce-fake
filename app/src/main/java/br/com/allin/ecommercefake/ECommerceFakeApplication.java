package br.com.allin.ecommercefake;

import android.app.Application;

import br.com.allin.ecommercefake.dao.AppDatabase;
import br.com.allin.ecommercefake.mvp.component.DaggerDatabaseComponent;
import br.com.allin.ecommercefake.mvp.module.DatabaseModule;
import br.com.allin.mobile.pushnotification.AlliNPush;
import br.com.allin.mobile.pushnotification.BTG360;

/**
 * Created by lucasrodrigues on 31/01/18.
 */

public class ECommerceFakeApplication extends Application {
    public static final String BTG_CODE = "15:1";

    private DatabaseModule databaseModule;

    @Override
    public void onCreate() {
        super.onCreate();

        setup();

        // Inicialização do BTG
        BTG360.initialize(this);

        // Inicialização da ferramenta de push
        AlliNPush.getInstance().registerForPushNotifications(this);
    }

    private void setup() {
        this.databaseModule = new DatabaseModule(this);

        DaggerDatabaseComponent.builder().databaseModule(databaseModule).build().inject(this);
    }

    public AppDatabase getAppDatabase() {
        return databaseModule.getAppDatabase();
    }
}